<?php
	session_start();

    include ("../database/database.php");

    $data = json_decode($_POST['parameter']);
    $part = $data->part;

    $query = "SELECT * FROM posts ORDER BY date DESC LIMIT 5 OFFSET " . (($part - 1) * 5);

    $result = $db->query($query);
    $num_result = $result->num_rows;

    $response = [];
    for ($i=0; $i < $num_result; $i++) {
        $row = $result->fetch_assoc();

        $title = $row["title"];
        $content = mb_substr($row["content"], 0, 150) . "...";
        $date = $row["date"];
        $img = $row["image"];
        $count_like = $row["count_like"];

        $response[$i] = [
            "title" => $title,
            "content" => $content,
            "date" => $date,
            "img" => $img,
            "count_like" => $count_like,
        ];
    }
    echo json_encode($response);
?>
